angular.module('WFApp',['ui.router']);

angular.module('WFApp').config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/state1");

    $stateProvider.state('state1',{
        url: '/state1',
        templateUrl: 'apex/nested'
    })
    .state('state2',{
        url: '/state2',
        templateUrl: 'apex/TestPage'
    });
});