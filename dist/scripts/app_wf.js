angular.module('wfapp', ['ngRoute', 'dndLists']);

angular.module('wfapp')
    .config(function($routeProvider){
        $routeProvider
            .when('/list',{
                templateUrl: 'apex/wf_list_html',
                controller: 'WorkflowController'
            })
            .when('/nested',{
                templateUrl: 'apex/wf_nested_home_html',
                controller: 'NestedController'
            })
            .otherwise({redirectTo: '/nested'})
    });