/**
 * The controller doesn't do much more than setting the initial data model
 */
//var app = angular.module("demo", ["ui.bootstrap.modal"]);

angular.module("demo").controller("NestedListsDemoController", function($scope) {



    $scope.open = function($event,domElement) {
      //$event.stopPropagation();
      $scope.showModal = true;
      $(domElement).modal('show');
    };


    $scope.close = function() {
      $scope.showModal = false;
    };

    $scope.cancel = function() {
      $scope.showModal = false;
    };

    $scope.editItem = function (item) {
        item.editing = true;
    }

    $scope.doneEditing = function (item) {
        item.editing = false;
        //dong some background ajax calling for persistence...
    };
    $scope.hoverIn = function(){
    this.hoverEdit = true;
    };

    $scope.hoverOut = function(){
        this.hoverEdit = false;
    };

    $scope.showGroup=function(element, itemgroup)
    {
        //console.log('datacontext '+$scope.datacontext);
        if (element == undefined) return false;
        //var datacontext=$(element).closest(".condition-element").attr("data-context");
        //if (datacontext == undefined) return false;
        //console.log('datacontext'+datacontext);
        if (element==itemgroup) return true;

        return false;
    }
    $scope.findGroup=function(itemtype)
    {
        
        //var datacontext=$("select").closest(".condition-element").attr("data-context");
        //$scope.datacontext=datacontext;
        var datacontext=itemtype;
        angular.forEach($scope.models.selected.querydefinition, function (queries) { 

            angular.forEach(queries, function (query) { 
            
                $scope.models.sectiontype=query.type;
                angular.forEach(query.columns, function (table) { 
            
                      angular.forEach(table, function (field) { 
                    
                         if (field.section=="") {
                            datacontext=$scope.models.sectiontype;
                            //field.section=$scope.models.sectiontype;
                           }
                    
                       });
            
               });
            
            });
            
        });
        //if (datacontext!="") item.section=datacontext;
        console.log('datacontext '+datacontext);
        console.log('data selected'+JSON.stringify($scope.models.selected));
        return datacontext;
    }
    
    //search thru the scope.models the Queries
    $scope.ListQueries = function(){
        var listobjects=[];        
        listobjects=$scope.recListQueries($scope.models.dropzones);
        console.log('data queries '+JSON.stringify(listobjects));
        $scope.listobjects=listobjects;
    }
    $scope.recListQueries = function(xobjects){
        var listobjects=[];
        angular.forEach(xobjects, function (item) { 
              if (item!=undefined)
              if ((item.processinloop!=undefined) && (item.processinloop==true)) {
                 listobjects.push({"label":item.label,"id":item.id.toString()});
              }else
              {
                if ((item instanceof Object) && (item!=$scope.models.selected)) {
                    var returnobjects=$scope.recListQueries(item);
                    if (returnobjects.length != 0) Array.prototype.push.apply(listobjects, returnobjects);
                }
              }
            });
        //console.log('data subqueries '+JSON.stringify(listobjects));
        return listobjects;
    }

    //search thru the scope.models the Variable of type Object
    $scope.ListVariablesObjects = function(){
        var listobjects=[];
        listobjects=$scope.recListVariablesObjects($scope.models.dropzones);
        console.log('data queries '+JSON.stringify(listobjects));
        $scope.listobjects=listobjects;
    }    
    $scope.recListVariablesObjects = function(xobjects){
        var listobjects=[];
        angular.forEach(xobjects, function (item) { 
              if (item!=undefined)
              if ((item.type!=undefined) && (item.type=="Variable")
                  && (item.format!=undefined) && (item.format=="object")
                  ) {
                 listobjects.push({"label":item.label,"id":item.id.toString()});
              }else
              {
                if ((item instanceof Object) && (item!=$scope.models.selected)) {
                    var returnobjects=$scope.recListVariablesObjects(item);
                    if (returnobjects.length != 0) Array.prototype.push.apply(listobjects, returnobjects);
                }
              }            
            });
        return listobjects;
    }

    //search thru the scope.models the Variable of type Object
    $scope.ListVariables = function(){
        var listobjects=[];
        listobjects=$scope.recListVariables($scope.models.dropzones);
        console.log('data queries '+JSON.stringify(listobjects));
        $scope.listvariables=listobjects;
    }    
    $scope.recListVariables = function(xobjects){
        var listobjects=[];
        angular.forEach(xobjects, function (item) { 
              if (item!=undefined)
              if ((item.type!=undefined) && (item.type=="Variable")
                  ) {
                 listobjects.push({"label":item.label,"id":item.id.toString()});
              }else
              {
                if ((item instanceof Object) && (item!=$scope.models.selected)) {
                    var returnobjects=$scope.recListVariables(item);
                    if (returnobjects.length != 0) Array.prototype.push.apply(listobjects, returnobjects);
                }
              }            
            });
        return listobjects;
    }

    //search thru the scope.models the Variable of type Object
    $scope.ListObjectVariables = function(){
        var listobjects=[];
        listobjects=$scope.recListObjectVariables($scope.models.selected);
        console.log('data object variables '+JSON.stringify(listobjects));
        $scope.listobjectvariables=listobjects;
    }
    $scope.recListObjectVariables = function(xobjects){
        var listobjects=[];
        angular.forEach(xobjects, function (item) { 
              if (item!=undefined)
              if ((item.type!=undefined) && (item.type=="MElement")
                  ) {
                 listobjects.push({"label":item.name,"id":item.id.toString()});
              }else
              {
                if (item instanceof Object){
                    var returnobjects=$scope.recListObjectVariables(item);
                    if (returnobjects.length != 0) Array.prototype.push.apply(listobjects, returnobjects);
                }
              }         
            });
        return listobjects;
    }



    //retrieve the Object Fields according to the Object table selected
    $scope.SelectObjectFields = function(itemtype){
        if (!(itemtype=='Field' || itemtype=='Content')) return;
        if ($scope.models== undefined || $scope.models == null) {
            return;
        }
        if ($scope.models.selected == undefined || $scope.models.selected == null) {
            return;
        }
        if ($scope.models.selected.object == undefined || $scope.models.selected.object == null) {
            return;
        }
        
        $scope.models.selectedfields=null;
        angular.forEach($scope.models.objects, function (table) {          
            if (table.name == $scope.models.selected.object) {
                $scope.models.selectedfields = table.fields;
            }
        });
        
        return;
    };
    
    var functiontype=null;
    //select function and return list of input
    $scope.selectedItemChanged = function(itemfunction,typefunction){
        //$scope.functiontype=null;
        itemfunction.value=null;
        if (itemfunction==undefined) return false;
        //fetch transform functions
        if (typefunction=='string')
        angular.forEach($scope.models.transformstringfunctions, function (functions) {          
            if (functions.type == itemfunction.tfunction) {
                if (itemfunction.value==undefined) itemfunction.value=$scope.selectedItemChangedValues(functions.value);
                
            }
        });
        if (typefunction=='math')
        angular.forEach($scope.models.transformmathfunctions, function (functions) {          
            if (functions.type == itemfunction.tfunction) {
                if (itemfunction.value==undefined) itemfunction.value=$scope.selectedItemChangedValues(functions.value);
                
            }
        });
        if (typefunction=='object')
        angular.forEach($scope.models.transformobjectfunctions, function (functions) {          
            if (functions.type == itemfunction.tfunction) {
                if (itemfunction.value==undefined) itemfunction.value=$scope.selectedItemChangedValues(functions.value);
                
            }
        });
        console.log('data function '+JSON.stringify(itemfunction.value));
       return true;
    }
   //create a new instance of the values object
   $scope.selectedItemChangedValues = function(values){
      var key;
      var valueobject={};
      for (key in values) {
          valueobject[key]="";
      }
      return valueobject;
    }
   //retrieve the icon path
   $scope.templateIconPath = function(xobjects){
        var listobjects='M26 14c0 6.627-5.373 12-12 12S2 20.627 2 14 7.373 2 14 2s12 5.373 12 12z';
        console.log('data path key:'+xobjects);
        angular.forEach($scope.models.templateicons, function (item) { 
              if (item!=undefined)
              if ((item.type!=undefined) && (item.type==xobjects)) {
                 listobjects=item.path;
              }
            });
        console.log('data path value:'+listobjects);
        return listobjects;
    } 
   
    $scope.models = {
        fieldindex:1000,
        selected: null,
        selectedfields: null,
        sectiontype:null,
        listobjects:null,
        templateicons:[
               {type:"Insert", path:"M13.68,9.448h-3.128V6.319c0-0.304-0.248-0.551-0.552-0.551S9.448,6.015,9.448,6.319v3.129H6.319c-0.304,0-0.551,0.247-0.551,0.551s0.247,0.551,0.551,0.551h3.129v3.129c0,0.305,0.248,0.551,0.552,0.551s0.552-0.246,0.552-0.551v-3.129h3.128c0.305,0,0.552-0.247,0.552-0.551S13.984,9.448,13.68,9.448z M10,0.968c-4.987,0-9.031,4.043-9.031,9.031c0,4.989,4.044,9.032,9.031,9.032c4.988,0,9.031-4.043,9.031-9.032C19.031,5.012,14.988,0.968,10,0.968z M10,17.902c-4.364,0-7.902-3.539-7.902-7.903c0-4.365,3.538-7.902,7.902-7.902S17.902,5.635,17.902,10C17.902,14.363,14.364,17.902,10,17.902z"},
               {type:"Delete", path:"M13.774,9.355h-7.36c-0.305,0-0.552,0.247-0.552,0.551s0.247,0.551,0.552,0.551h7.36c0.304,0,0.551-0.247,0.551-0.551S14.078,9.355,13.774,9.355z M10.094,0.875c-4.988,0-9.031,4.043-9.031,9.031s4.043,9.031,9.031,9.031s9.031-4.043,9.031-9.031S15.082,0.875,10.094,0.875z M10.094,17.809c-4.365,0-7.902-3.538-7.902-7.902c0-4.365,3.538-7.902,7.902-7.902c4.364,0,7.902,3.538,7.902,7.902C17.996,14.271,14.458,17.809,10.094,17.809z"},
               {type:"Update", path:"M9.917,0.875c-5.086,0-9.208,4.123-9.208,9.208c0,5.086,4.123,9.208,9.208,9.208s9.208-4.122,9.208-9.208C19.125,4.998,15.003,0.875,9.917,0.875z M9.917,18.141c-4.451,0-8.058-3.607-8.058-8.058s3.607-8.057,8.058-8.057c4.449,0,8.057,3.607,8.057,8.057S14.366,18.141,9.917,18.141z M13.851,6.794l-5.373,5.372L5.984,9.672c-0.219-0.219-0.575-0.219-0.795,0c-0.219,0.22-0.219,0.575,0,0.794l2.823,2.823c0.02,0.028,0.031,0.059,0.055,0.083c0.113,0.113,0.263,0.166,0.411,0.162c0.148,0.004,0.298-0.049,0.411-0.162c0.024-0.024,0.036-0.055,0.055-0.083l5.701-5.7c0.219-0.219,0.219-0.575,0-0.794C14.425,6.575,14.069,6.575,13.851,6.794z"},
               {type:"Query", path:"M9.896,3.838L0.792,1.562v14.794l9.104,2.276L19,16.356V1.562L9.896,3.838z M9.327,17.332L1.93,15.219V3.27l7.397,1.585V17.332z M17.862,15.219l-7.397,2.113V4.855l7.397-1.585V15.219z"},
               {type:"Ftp", path:"M17.896,12.706v-0.005v-0.003L15.855,2.507c-0.046-0.24-0.255-0.413-0.5-0.413H4.899c-0.24,0-0.447,0.166-0.498,0.4L2.106,12.696c-0.008,0.035-0.013,0.071-0.013,0.107v4.593c0,0.28,0.229,0.51,0.51,0.51h14.792c0.28,0,0.51-0.229,0.51-0.51v-4.593C17.906,12.77,17.904,12.737,17.896,12.706 M5.31,3.114h9.625l1.842,9.179h-4.481c-0.28,0-0.51,0.229-0.51,0.511c0,0.703-1.081,1.546-1.785,1.546c-0.704,0-1.785-0.843-1.785-1.546c0-0.281-0.229-0.511-0.51-0.511H3.239L5.31,3.114zM16.886,16.886H3.114v-3.572H7.25c0.235,1.021,1.658,2.032,2.75,2.032c1.092,0,2.515-1.012,2.749-2.032h4.137V16.886z"},
               {type:"Forms", path:"M15.475,6.692l-4.084-4.083C11.32,2.538,11.223,2.5,11.125,2.5h-6c-0.413,0-0.75,0.337-0.75,0.75v13.5c0,0.412,0.337,0.75,0.75,0.75h9.75c0.412,0,0.75-0.338,0.75-0.75V6.94C15.609,6.839,15.554,6.771,15.475,6.692 M11.5,3.779l2.843,2.846H11.5V3.779z M14.875,16.75h-9.75V3.25h5.625V7c0,0.206,0.168,0.375,0.375,0.375h3.75V16.75z"},
               {type:"Variable", path:"M10.292,4.229c-1.487,0-2.691,1.205-2.691,2.691s1.205,2.691,2.691,2.691s2.69-1.205,2.69-2.691S11.779,4.229,10.292,4.229z M10.292,8.535c-0.892,0-1.615-0.723-1.615-1.615S9.4,5.306,10.292,5.306c0.891,0,1.614,0.722,1.614,1.614S11.184,8.535,10.292,8.535z M10.292,1C6.725,1,3.834,3.892,3.834,7.458c0,3.567,6.458,10.764,6.458,10.764s6.458-7.196,6.458-10.764C16.75,3.892,13.859,1,10.292,1z M4.91,7.525c0-3.009,2.41-5.449,5.382-5.449c2.971,0,5.381,2.44,5.381,5.449s-5.381,9.082-5.381,9.082S4.91,10.535,4.91,7.525z"},
               {type:"Break", path:"M14.989,9.491L6.071,0.537C5.78,0.246,5.308,0.244,5.017,0.535c-0.294,0.29-0.294,0.763-0.003,1.054l8.394,8.428L5.014,18.41c-0.291,0.291-0.291,0.763,0,1.054c0.146,0.146,0.335,0.218,0.527,0.218c0.19,0,0.382-0.073,0.527-0.218l8.918-8.919C15.277,10.254,15.277,9.784,14.989,9.491z"},
               {type:"Documents", path:"M18.378,1.062H3.855c-0.309,0-0.559,0.25-0.559,0.559c0,0.309,0.25,0.559,0.559,0.559h13.964v13.964c0,0.309,0.25,0.559,0.559,0.559c0.31,0,0.56-0.25,0.56-0.559V1.621C18.938,1.312,18.688,1.062,18.378,1.062z M16.144,3.296H1.621c-0.309,0-0.559,0.25-0.559,0.559v14.523c0,0.31,0.25,0.56,0.559,0.56h14.523c0.309,0,0.559-0.25,0.559-0.56V3.855C16.702,3.546,16.452,3.296,16.144,3.296z M15.586,17.262c0,0.31-0.25,0.558-0.56,0.558H2.738c-0.309,0-0.559-0.248-0.559-0.558V4.972c0-0.309,0.25-0.559,0.559-0.559h12.289c0.31,0,0.56,0.25,0.56,0.559V17.262z"},
               {type:"Condition", path:"M14.68,12.621c-0.9,0-1.702,0.43-2.216,1.09l-4.549-2.637c0.284-0.691,0.284-1.457,0-2.146l4.549-2.638c0.514,0.661,1.315,1.09,2.216,1.09c1.549,0,2.809-1.26,2.809-2.808c0-1.548-1.26-2.809-2.809-2.809c-1.548,0-2.808,1.26-2.808,2.809c0,0.38,0.076,0.741,0.214,1.073l-4.55,2.638c-0.515-0.661-1.316-1.09-2.217-1.09c-1.548,0-2.808,1.26-2.808,2.809s1.26,2.808,2.808,2.808c0.9,0,1.702-0.43,2.217-1.09l4.55,2.637c-0.138,0.332-0.214,0.693-0.214,1.074c0,1.549,1.26,2.809,2.808,2.809c1.549,0,2.809-1.26,2.809-2.809S16.229,12.621,14.68,12.621M14.68,2.512c1.136,0,2.06,0.923,2.06,2.06S15.815,6.63,14.68,6.63s-2.059-0.923-2.059-2.059S13.544,2.512,14.68,2.512M5.319,12.061c-1.136,0-2.06-0.924-2.06-2.06s0.923-2.059,2.06-2.059c1.135,0,2.06,0.923,2.06,2.059S6.454,12.061,5.319,12.061M14.68,17.488c-1.136,0-2.059-0.922-2.059-2.059s0.923-2.061,2.059-2.061s2.06,0.924,2.06,2.061S15.815,17.488,14.68,17.488"},
               {type:"Loop", path:"M3.254,6.572c0.008,0.072,0.048,0.123,0.082,0.187c0.036,0.07,0.06,0.137,0.12,0.187C3.47,6.957,3.47,6.978,3.484,6.988c0.048,0.034,0.108,0.018,0.162,0.035c0.057,0.019,0.1,0.066,0.164,0.066c0.004,0,0.01,0,0.015,0l2.934-0.074c0.317-0.007,0.568-0.271,0.56-0.589C7.311,6.113,7.055,5.865,6.744,5.865c-0.005,0-0.01,0-0.015,0L5.074,5.907c2.146-2.118,5.604-2.634,7.971-1.007c2.775,1.912,3.48,5.726,1.57,8.501c-1.912,2.781-5.729,3.486-8.507,1.572c-0.259-0.18-0.618-0.119-0.799,0.146c-0.18,0.262-0.114,0.621,0.148,0.801c1.254,0.863,2.687,1.279,4.106,1.279c2.313,0,4.591-1.1,6.001-3.146c2.268-3.297,1.432-7.829-1.867-10.101c-2.781-1.913-6.816-1.36-9.351,1.058L4.309,3.567C4.303,3.252,4.036,3.069,3.72,3.007C3.402,3.015,3.151,3.279,3.16,3.597l0.075,2.932C3.234,6.547,3.251,6.556,3.254,6.572z"},
               {type:"Exit", path:"M1.729,9.212h14.656l-4.184-4.184c-0.307-0.306-0.307-0.801,0-1.107c0.305-0.306,0.801-0.306,1.106,0l5.481,5.482c0.018,0.014,0.037,0.019,0.053,0.034c0.181,0.181,0.242,0.425,0.209,0.66c-0.004,0.038-0.012,0.071-0.021,0.109c-0.028,0.098-0.075,0.188-0.143,0.271c-0.021,0.026-0.021,0.061-0.045,0.085c-0.015,0.016-0.034,0.02-0.051,0.033l-5.483,5.483c-0.306,0.307-0.802,0.307-1.106,0c-0.307-0.305-0.307-0.801,0-1.105l4.184-4.185H1.729c-0.436,0-0.788-0.353-0.788-0.788S1.293,9.212,1.729,9.212z"},
               {type:"Container", path:"M17.701,3.919H2.299c-0.223,0-0.405,0.183-0.405,0.405v11.349c0,0.223,0.183,0.406,0.405,0.406h15.402c0.224,0,0.405-0.184,0.405-0.406V4.325C18.106,4.102,17.925,3.919,17.701,3.919 M17.296,15.268H2.704V7.162h14.592V15.268zM17.296,6.352H2.704V4.73h14.592V6.352z M5.947,5.541c0,0.223-0.183,0.405-0.405,0.405H3.515c-0.223,0-0.405-0.182-0.405-0.405c0-0.223,0.183-0.405,0.405-0.405h2.027C5.764,5.135,5.947,5.318,5.947,5.541"},
               {type:"Transform", path:"M12.319,5.792L8.836,2.328C8.589,2.08,8.269,2.295,8.269,2.573v1.534C8.115,4.091,7.937,4.084,7.783,4.084c-2.592,0-4.7,2.097-4.7,4.676c0,1.749,0.968,3.337,2.528,4.146c0.352,0.194,0.651-0.257,0.424-0.529c-0.415-0.492-0.643-1.118-0.643-1.762c0-1.514,1.261-2.747,2.787-2.747c0.029,0,0.06,0,0.09,0.002v1.632c0,0.335,0.378,0.435,0.568,0.245l3.483-3.464C12.455,6.147,12.455,5.928,12.319,5.792 M8.938,8.67V7.554c0-0.411-0.528-0.377-0.781-0.377c-1.906,0-3.457,1.542-3.457,3.438c0,0.271,0.033,0.542,0.097,0.805C4.149,10.7,3.775,9.762,3.775,8.76c0-2.197,1.798-3.985,4.008-3.985c0.251,0,0.501,0.023,0.744,0.069c0.212,0.039,0.412-0.124,0.412-0.34v-1.1l2.646,2.633L8.938,8.67z M14.389,7.107c-0.34-0.18-0.662,0.244-0.424,0.529c0.416,0.493,0.644,1.118,0.644,1.762c0,1.515-1.272,2.747-2.798,2.747c-0.029,0-0.061,0-0.089-0.002v-1.631c0-0.354-0.382-0.419-0.558-0.246l-3.482,3.465c-0.136,0.136-0.136,0.355,0,0.49l3.482,3.465c0.189,0.186,0.568,0.096,0.568-0.245v-1.533c0.153,0.016,0.331,0.022,0.484,0.022c2.592,0,4.7-2.098,4.7-4.677C16.917,9.506,15.948,7.917,14.389,7.107 M12.217,15.238c-0.251,0-0.501-0.022-0.743-0.069c-0.212-0.039-0.411,0.125-0.411,0.341v1.101l-2.646-2.634l2.646-2.633v1.116c0,0.174,0.126,0.318,0.295,0.343c0.158,0.024,0.318,0.034,0.486,0.034c1.905,0,3.456-1.542,3.456-3.438c0-0.271-0.032-0.541-0.097-0.804c0.648,0.719,1.022,1.659,1.022,2.66C16.226,13.451,14.428,15.238,12.217,15.238"},
               {type:"Connection", path:"M17.308,7.564h-1.993c0-2.929-2.385-5.314-5.314-5.314S4.686,4.635,4.686,7.564H2.693c-0.244,0-0.443,0.2-0.443,0.443v9.3c0,0.243,0.199,0.442,0.443,0.442h14.615c0.243,0,0.442-0.199,0.442-0.442v-9.3C17.75,7.764,17.551,7.564,17.308,7.564 M10,3.136c2.442,0,4.43,1.986,4.43,4.428H5.571C5.571,5.122,7.558,3.136,10,3.136 M16.865,16.864H3.136V8.45h13.729V16.864z M10,10.664c-0.854,0-1.55,0.696-1.55,1.551c0,0.699,0.467,1.292,1.107,1.485v0.95c0,0.243,0.2,0.442,0.443,0.442s0.443-0.199,0.443-0.442V13.7c0.64-0.193,1.106-0.786,1.106-1.485C11.55,11.36,10.854,10.664,10,10.664 M10,12.878c-0.366,0-0.664-0.298-0.664-0.663c0-0.366,0.298-0.665,0.664-0.665c0.365,0,0.664,0.299,0.664,0.665C10.664,12.58,10.365,12.878,10,12.878"},
               {type:"Callout", path:"M16.889,8.82c0.002-0.038,0.006-0.075,0.006-0.112c0-1.427-1.158-2.585-2.586-2.585c-0.65,0-1.244,0.243-1.699,0.641c-0.92-1.421-2.513-2.364-4.333-2.364c-2.595,0-4.738,1.914-5.108,4.406c-1.518,0.361-2.648,1.722-2.648,3.35c0,1.904,1.543,3.447,3.447,3.447h12.065c1.904,0,3.447-1.543,3.447-3.447C19.48,10.547,18.377,9.201,16.889,8.82 M16.033,14.74H3.968c-1.426,0-2.585-1.16-2.585-2.586c0-1.2,0.816-2.233,1.985-2.512C3.71,9.561,3.969,9.279,4.021,8.931C4.333,6.838,6.162,5.26,8.277,5.26c1.461,0,2.811,0.736,3.61,1.971c0.135,0.21,0.355,0.351,0.604,0.385c0.039,0.006,0.08,0.008,0.119,0.008c0.207,0,0.408-0.074,0.566-0.212c0.316-0.275,0.719-0.428,1.133-0.428c0.951,0,1.725,0.773,1.723,1.733L16.027,8.78c-0.018,0.408,0.252,0.772,0.646,0.874c1.146,0.293,1.945,1.321,1.945,2.5C18.619,13.58,17.459,14.74,16.033,14.74"}
              ],
        templates: [
            {type: "Query", id: 4, processinloop:true, querytype:"Query", querydefinition: [[{"type": "Select","title": "Select","id": 1,"columns": [[]]},{"type": "Where","title": "Where","id": 1,"columns": [[]]},{"type": "Order","title": "Order","id": 2,"columns": [[]]}]]},
            {type: "Insert", id: 5},
            {type: "Delete", id: 6},
            {type: "Update", id: 7},
            {type: "Connection", id: 7},
            {type: "Transform", id: 90, transformdefinition: [[]]},
            {type: "Ftp", id: 17, processinloop:true},
            {type: "Forms", id: 18},
            {type: "Documents", processinloop:true, id: 18},
            {type: "Callout", id: 8, querydefinition: [[{"type": "Body","title": "Body","id": 1,"columns": [[]]},{"type": "Response","title": "Response","id": 1,"columns": [[]]}]]},
            {type: "Variable", id: 9, format:"", label:"", transformdefinition: [[]] , objectdefinition: [[]]},
            {type: "Break", id: 10},
            {type: "Exit", id: 11},
            {type: "Container", id: 1, columns: [[], []]},
            {type: "Condition", id: 1, filters:[[], []], columns: [[], []]},
            {type: "Loop", id: 1, columns: [[], []]}
        ],
        templatereturntypes:[
            {type: "Get", title:"Get", id: 12},
            {type: "Set", title:"Set", id: 120},
            {type: "Private", title:"Private", id: 120},
            {type: "Public", title:"Public", id: 120}
        ],
        templatecallout: [
            {type: "Content", title:"Content", section:"", id: 12}
        ],
        templatetransform: [
            {type: "STFunction", title:"String", section:"", id: 12},
            {type: "MTFunction", title:"Math", section:"", id: 12},
            {type: "OTFunction", title:"Object", section:"", id: 12},
            {type: "Transformation",title: "Transform",id: 1, columns: [[{type:"Assign",object:"",assignto:""}]]}
        ],
        templatequery: [
            {type: "Field", title:"Field", section:"", id: 12}
        ],
        templateobjects: [
            {type: "MElement", title:"Element", format:"", elementtype:"", name:"",id: 12},
            {type: "MRecord", title:"Record", id: 1, columns: [[]]},
            {type: "MArray", title:"Array", id: 1, columns: [[]]},
            {type: "MMethod", title:"Method", id: 1, columns: [[{type:"MReturn",returntype:"Get",get:"",set:""}]]},
            {type: "SFunction", title:"String", section:"", id: 12},
            {type: "MFunction", title:"Math", section:"", id: 14},
            {type: "OFunction", title:"Object", section:"", id: 12},
            {type: "MTransformation",title: "Transform",id: 1, columns: [[{type:"MAssign",object:"",assignto:""}]]}
        ],
        templateconditions: [
            {type: "Filter", title:"Filter", id: 12},
            {type: "GroupAnd", title:"And Group", id: 1, columns: [[]]},
            {type: "GroupOr", title:"Or Group", id: 1, columns: [[]]}
        ],
        calloutresponses:[
                {type:"200"},
                {type:"400"},
                {type:"401"},
                {type:"500"}
                ],
        calloutmethods:[
                {type:"Get"},
                {type:"Put"},
                {type:"Patch"},
                {type:"Delete"},
                {type:"Post"},
                {type:"Head"}
                ],
        calloutauthorization:[
                {type:"Oath"},
                {type:"Token"},
                {type:"Password"},
                {type:"None"}
                ],
        calloutapis:[
                {type:"Soap"},
                {type:"Rest"}
                ],
        calloutformats:[
                {type:"Inbound"},
                {type:"Outbound"}
                ],
        sort:[
                {type:"Desc"},
                {type:"Asc"}
                ],
        querytype:[
                {type:"Query"},
                {type:"Describe"},
                {type:"Object"}
                ],
        fromtos: [
                 {type:"value or variables"},
                 {type:"first"},
                 {type:"last"}
                 ],
        operators: [
                 {type:"or"},
                 {type:"and"},
                 {type:"xor"},
                 {type:"not"}
                 ],
        elementtypes:[
                {type:"Private"},
                {type:"Public"}
                ],
        variables:[
                {type:"decimal"},
                {type:"string"},
                {type:"double"},
                {type:"long"},
                {type:"integer"},
                {type:"array"},
                {type:"object"}
                ],
        objecttypes:[
                {type:"single"},
                {type:"list"},
                {type:"map"},
                {type:"set"}
                ],
        ftpoperations:[
                {type:"download"},
                {type:"upload"},
                {type:"delete"},
                {type:"list"},
                {type:"create folder"},
                {type:"delete folder"}
                ],
        objects: [
                {name:"akatia__shipment__c", fields:[{name:"Id"}, {name:"Firstname__c"},{name:"Lastname__c"}]},
                {name:"akatia__shipment_item__c", fields:[]},
                {name:"akatia__recceipts__c", fields:[]},
                {name:"akatia__receipt_item__c", fields:[]},
                {name:"akatia__inventory__c", fields:[]},
                {name:"akatia__invoice__c", fields:[]},
                {name:"Account", fields:[]},
                {name:"Contact", fields:[]}
        ],
        conditions:[
                    {type:"equal"},
                    {type:"not equal"},
                    {type:"greater than"},
                    {type:"greater or equal than"},
                    {type:"lower than"},
                    {type:"lower or equal than"},
                    {type:"starts with"},
                    {type:"contains"},
                    {type:"ends with"},
                    {type:"between"}
                ],
        transformstringfunctions:[
                    {type:"replace", return:"string", value:{"Regex":"","Replacement":""}},
                    {type:"toUpperCase", return:"string", value:{}},
                    {type:"toLowerCase", return:"string", value:{}},
                    {type:"length", return:"integer", value:{}},
                    {type:"toString", return:"string", value:{}},
                    {type:"valueOf", return:"string", value:{"Primitive":""}},
                    {type:"trim", return:"string", value:{}},
                    {type:"substring", return:"string", value:{"Begin":"","End":""}},
                    {type:"startsWith", return:"string", value:{"Prefix":"","ToOffset":""}},
                    {type:"replaceAll", return:"string", value:{"Regex":"","Replacement":""}},
                    {type:"matches", return:"boolean", value:{"Regex":""}},
                    {type:"indexOf", return:"integer", value:{"String":"","FromIndex":""}},
                    {type:"add", return:"string", value:{"String":""}},
                    ],
        transformmathfunctions:[
                    {type:"add", return:"string", value:{"Value":""}},
                    {type:"remove", return:"string", value:{"Value":""}},
                    {type:"multiple", return:"string", value:{"By":""}},
                    {type:"divide", return:"string", value:{"By":""}},
                    {type:"round", return:"string", value:{"By":""}},
                    {type:"floor", return:"string", value:{"Value":""}},
                    {type:"ceiling", return:"string", value:{"Value":""}},                    
                    {type:"mod", return:"string", value:{"Value":""}}
                    ],   
        transformobjectfunctions:[
                    {type:"add", return:"string", value:{"Key":"","Value":""}},
                    {type:"remove", return:"", value:{"Key":""}},
                    {type:"size", return:"integer", value:{}},
                    {type:"contains", return:"boolean", value:{"Key":""}}                  
                    ],                        
        objectconditions:[
                    {type:"equal"},
                    {type:"not equal"},
                    {type:"greater than"},
                    {type:"greater or equal than"},
                    {type:"lower than"},
                    {type:"lower or equal than"},
                    {type:"starts with"},
                    {type:"contains"},
                    {type:"ends with"},
                    {type:"between"},
                    {type:"in"},
                    {type:"not in"},
                    {type:"express it yourself"}

                ],
        dropzones: {
                      "Hubspot campaign creation and send": [
                        {
                          "type": "Variable",
                          "id": 9,
                          "format": "object",
                          "label": "Hubspot Contact",
                          "transformdefinition": [
                            []
                          ],
                          "objectdefinition": [
                            [
                              {
                                "type": "MArray",
                                "title": "Array",
                                "id": 1,
                                "columns": [
                                  [
                                    {
                                      "type": "MRecord",
                                      "title": "Record",
                                      "id": 2,
                                      "columns": [
                                        [
                                          {
                                            "type": "MElement",
                                            "title": "Element",
                                            "format": "string",
                                            "elementtype": "Public",
                                            "name": "property",
                                            "id": 12,
                                            "show": true
                                          },
                                          {
                                            "type": "MElement",
                                            "title": "Element",
                                            "format": "string",
                                            "elementtype": "Public",
                                            "name": "value",
                                            "id": 13,
                                            "show": true
                                          }
                                        ]
                                      ]
                                    }
                                  ]
                                ],
                                "show": false,
                                "editing": false,
                                "label": "properties"
                              }
                            ]
                          ],
                          "objecttype": "list"
                        },
                        {
                          "type": "Variable",
                          "id": 9,
                          "format": "object",
                          "label": "Get Hubspot Template",
                          "transformdefinition": [
                            []
                          ],
                          "objectdefinition": [
                            [
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "integer",
                                "elementtype": "Public",
                                "name": "category_id",
                                "id": 12,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "cdn_minified_url",
                                "id": 13,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "cdn_url",
                                "id": 14,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "integer",
                                "elementtype": "Public",
                                "name": "deleted_at",
                                "id": 15,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "folder",
                                "id": 16,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "integer",
                                "elementtype": "Public",
                                "name": "generated_from_layout_id",
                                "id": 17,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "integer",
                                "elementtype": "Public",
                                "name": "id",
                                "id": 18,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "boolean",
                                "elementtype": "Public",
                                "name": "is_available_for_new_content",
                                "id": 19,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "boolean",
                                "elementtype": "Public",
                                "name": "is_from_layout",
                                "id": 20,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "boolean",
                                "elementtype": "Public",
                                "name": "is_read_only",
                                "id": 21,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "label",
                                "id": 22,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "linked_style_id",
                                "id": 23,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "path",
                                "id": 24,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "integer",
                                "elementtype": "Public",
                                "name": "portal_id",
                                "id": 25,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "source",
                                "id": 26,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "template_type",
                                "id": 27,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "integer",
                                "elementtype": "Public",
                                "name": "thumbnail_width",
                                "id": 28,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "type",
                                "id": 29,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "double",
                                "elementtype": "Public",
                                "name": "updated",
                                "id": 30,
                                "show": true
                              },
                              {
                                "type": "MElement",
                                "title": "Element",
                                "format": "string",
                                "elementtype": "Public",
                                "name": "updated_by",
                                "id": 31,
                                "show": true
                              }
                            ]
                          ],
                          "objecttype": "single"
                        },
                        {
                          "type": "Callout",
                          "id": 8,
                          "querydefinition": [
                            [
                              {
                                "type": "Body",
                                "title": "Body",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              },
                              {
                                "type": "Response",
                                "title": "Response",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              }
                            ]
                          ],
                          "label": "Get Hubspot Templates",
                          "format": "Inbound",
                          "api": "Rest",
                          "path": "http://api.hubapi.com/content/api/v2/templates/359393980?hapikey=demo",
                          "authorization": "Oath"
                        },
                        {
                          "type": "Connection",
                          "id": 7,
                          "label": "SDFC Marketing",
                          "path": "https://tes.salesforce.com",
                          "username": "fghhhf",
                          "password": "errrrrre",
                          "consumer": "Mrrww@3423552@2@2",
                          "secret": "345555553344433",
                          "grant": "password",
                          "token": "SDFC Marketing"
                        },
                        {
                          "type": "Query",
                          "id": 4,
                          "processinloop": true,
                          "querytype": "Query",
                          "querydefinition": [
                            [
                              {
                                "type": "Select",
                                "title": "Select",
                                "id": 1,
                                "columns": [
                                  [
                                    {
                                      "type": "Field",
                                      "title": "Field",
                                      "section": "Select",
                                      "id": 12
                                    }
                                  ]
                                ]
                              },
                              {
                                "type": "Where",
                                "title": "Where",
                                "id": 1,
                                "columns": [
                                  [
                                    {
                                      "type": "Field",
                                      "title": "Field",
                                      "section": "Where",
                                      "id": 13
                                    }
                                  ]
                                ]
                              },
                              {
                                "type": "Order",
                                "title": "Order",
                                "id": 2,
                                "columns": [
                                  []
                                ]
                              }
                            ]
                          ],
                          "label": "Read Campaign",
                          "object": "Contact",
                          "limit": "1"
                        },
                        {
                          "type": "Query",
                          "id": 6,
                          "processinloop": true,
                          "querytype": "Describe",
                          "querydefinition": [
                            [
                              {
                                "type": "Select",
                                "title": "Select",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              },
                              {
                                "type": "Where",
                                "title": "Where",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              },
                              {
                                "type": "Order",
                                "title": "Order",
                                "id": 2,
                                "columns": [
                                  []
                                ]
                              }
                            ]
                          ],
                          "label": "Read Campaign Members",
                          "object": "Account"
                        },
                        {
                          "type": "Query",
                          "id": 5,
                          "processinloop": true,
                          "querytype": "Query",
                          "querydefinition": [
                            [
                              {
                                "type": "Select",
                                "title": "Select",
                                "id": 1,
                                "columns": [
                                  [
                                    {
                                      "type": "Field",
                                      "title": "Field",
                                      "section": "Select",
                                      "id": 16
                                    },
                                    {
                                      "type": "Field",
                                      "title": "Field",
                                      "section": "Select",
                                      "id": 15
                                    },
                                    {
                                      "type": "Field",
                                      "title": "Field",
                                      "section": "Select",
                                      "id": 14
                                    }
                                  ]
                                ]
                              },
                              {
                                "type": "Where",
                                "title": "Where",
                                "id": 1,
                                "columns": [
                                  [
                                    {
                                      "type": "Field",
                                      "title": "Field",
                                      "section": "Where",
                                      "id": 17,
                                      "format": "in",
                                      "inornot": "Read Contacts in Campaign Members"
                                    }
                                  ]
                                ]
                              },
                              {
                                "type": "Order",
                                "title": "Order",
                                "id": 2,
                                "columns": [
                                  []
                                ]
                              }
                            ]
                          ],
                          "label": "Read Contacts in Campaign Members",
                          "object": "Contact"
                        },
                        {
                          "type": "Loop",
                          "id": 1,
                          "columns": [
                            [
                              {
                                "type": "Transform",
                                "id": 90,
                                "transformdefinition": [
                                  [
                                    {
                                      "type": "Transformation",
                                      "title": "Transform",
                                      "id": 1,
                                      "columns": [
                                        [
                                          {
                                            "type": "Assign",
                                            "object": "Hubspot Contact",
                                            "assignto": "",
                                            "assignedto": "Hubspot Contact",
                                            "show": true
                                          },
                                          {
                                            "type": "OTFunction",
                                            "title": "Object",
                                            "section": "",
                                            "id": 12,
                                            "tfunction": "add",
                                            "value": {
                                              "Value": ""
                                            },
                                            "show": true
                                          }
                                        ]
                                      ],
                                      "show": false
                                    },
                                    {
                                      "type": "Transformation",
                                      "title": "Transform",
                                      "id": 2,
                                      "columns": [
                                        [
                                          {
                                            "type": "Assign",
                                            "object": "Hubspot Contact",
                                            "assignto": "",
                                            "assignedto": "Hubspot Contact",
                                            "show": true
                                          },
                                          {
                                            "type": "OTFunction",
                                            "title": "Object",
                                            "section": "",
                                            "id": 14,
                                            "tfunction": "add",
                                            "value": {
                                              "Value": ""
                                            },
                                            "show": true
                                          }
                                        ]
                                      ],
                                      "show": false
                                    },
                                    {
                                      "type": "Transformation",
                                      "title": "Transform",
                                      "id": 3,
                                      "columns": [
                                        [
                                          {
                                            "type": "Assign",
                                            "object": "Hubspot Contact",
                                            "assignto": "",
                                            "assignedto": "Hubspot Contact",
                                            "show": true
                                          },
                                          {
                                            "type": "OTFunction",
                                            "title": "Object",
                                            "section": "",
                                            "id": 15,
                                            "tfunction": "add",
                                            "value": {
                                              "Value": ""
                                            },
                                            "show": true
                                          }
                                        ]
                                      ]
                                    }
                                  ]
                                ],
                                "label": "Load Contact Info"
                              },
                              {
                                "type": "Callout",
                                "id": 9,
                                "querydefinition": [
                                  [
                                    {
                                      "type": "Body",
                                      "title": "Body",
                                      "id": 1,
                                      "columns": [
                                        []
                                      ]
                                    },
                                    {
                                      "type": "Response",
                                      "title": "Response",
                                      "id": 1,
                                      "columns": [
                                        []
                                      ]
                                    }
                                  ]
                                ],
                                "label": "Create Hubspot Contact",
                                "format": "Outbound",
                                "method": "Post",
                                "api": "Rest",
                                "authorization": "Oath",
                                "path": "https://api.hubapi.com/contacts/v1/contact/?hapikey=demo"
                              }
                            ],
                            [
                              {
                                "type": "Transform",
                                "id": 91,
                                "transformdefinition": [
                                  []
                                ],
                                "label": "Update SDFC Contact Hubspot Id"
                              }
                            ]
                          ],
                          "show": false,
                          "label": "All the Contacts in Campaign",
                          "from": "first",
                          "to": "last",
                          "object": "Read Contacts in Campaign Members"
                        },
                        {
                          "type": "Update",
                          "id": 7,
                          "label": "SFDC Contacts",
                          "object": "Hubspot Contact"
                        },
                        {
                          "type": "Callout",
                          "id": 11,
                          "querydefinition": [
                            [
                              {
                                "type": "Body",
                                "title": "Body",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              },
                              {
                                "type": "Response",
                                "title": "Response",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              }
                            ]
                          ],
                          "label": "Create the Hubspot Campaign",
                          "format": "Outbound",
                          "method": "Post",
                          "api": "Rest"
                        },
                        {
                          "type": "Callout",
                          "id": 10,
                          "querydefinition": [
                            [
                              {
                                "type": "Body",
                                "title": "Body",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              },
                              {
                                "type": "Response",
                                "title": "Response",
                                "id": 1,
                                "columns": [
                                  []
                                ]
                              }
                            ]
                          ],
                          "label": "Run Hubspot Campaign",
                          "format": "Outbound",
                          "method": "Post",
                          "api": "Rest",
                          "authorization": "Oath"
                        }
                      ]
                    }
    };

    $scope.$watch('models.dropzones', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);

});
