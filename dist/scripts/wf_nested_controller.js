angular.module("wfapp")
    .controller("NestedController",["$scope","ServiceConfigurator",function($scope,ServiceConfigurator){

    function Init(){
        $scope.config = {};
        $scope.config.templateIcons = ServiceConfigurator.templateIcons;
        $scope.config.templates = ServiceConfigurator.templates;
    }

    Init();
}]);