var sass = require('node-sass');
var fs = require('fs');

sass.render({
  file: 'styles/main.scss',
  //compressed, nested, expanded, compact
  outputStyle: 'expanded',
  outFile: 'styles/dist/style.css',
  sourceMap: true,
},function(err,result){
    console.log(sass.info);
    console.log(result.css);
    console.log(result.map);
    console.log(result.stats);
    fs.writeFile('styles/dist/style.css',result.css.toString(),function(err){
      if(err) throw err;
      console.log('The file has been saved');
    });
});