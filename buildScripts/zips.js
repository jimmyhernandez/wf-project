var fs = require('fs');
var archiver = require('archiver');
var mv = require('mv');

var output = fs.createWriteStream('dist/deploysf/staticResources/sfappresources.resource');
var archive = archiver('zip');

output.on('close',function(){
  console.log(archive.pointer() + ' total bytes');
  console.log('archiver has been finalized and the output file descriptor has closed.');
});

archive.on('error', function(err) {
  throw err;
});

archive.pipe(output);

// append files from a directory 
archive.directory('dist/libs/');
archive.directory('dist/styles/');
archive.directory('dist/scripts/');
// finalize the archive (ie we are done appending files but streams have to finish yet) 
archive.finalize();

/*mv('dist/sfappresources.resource', 'src/staticResources/sfappresources.resource', function(err) {
    if(err){
        console.log(err);
    }
});*/