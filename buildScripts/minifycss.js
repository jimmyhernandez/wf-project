var compressor = require('node-minify');

compressor.minify({
  compressor: 'clean-css',
  input: ['node_modules/bootstrap/dist/css/bootstrap.css','styles/dist/style.css'],
  output: 'dist/styles/main.css',
  options: {
    advanced: false, // set to false to disable advanced optimizations - selector & property merging, reduction, etc. 
    aggressiveMerging: false // set to false to disable aggressive merging of properties. 
  },
  callback: function (err, min) {
    if(err){
        console.log(err);
    }else{
        console.log('Success!!!');
    }
  }
});